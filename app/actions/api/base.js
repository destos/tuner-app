/* @flow */
import partial from 'lodash/fp/partial';
import assign from 'lodash/fp/assign';
import isFunction from 'lodash/fp/isFunction';
import { normalize as normalizeFunction, schema } from 'normalizr';
import { v4 } from 'node-uuid';
import { onAction } from 'redux-action-watch/lib/actionCreators';
import { WEBSOCKET_SEND, WEBSOCKET_MESSAGE } from '../../middleware/websocket';

// Base API action builder for models synced over

// Actions should all have timestamps, timestamps are sent in the WS.
// Should we use JWT for all messages so a user auth info can be sent along?
// How much overhead does that use and will it affect performance?
// TODO: handle subscribes and un-subs
// TODO: to handle unsubscribes we would need a new unsub mixin/action added to channels-api

export const defaultConfig = {
  stream: null,
  // in case you want to provide a different name for actions
  actionName: null,
  normalize: null,
  actions: [
    { name: 'create' },
    { name: 'retrieve' },
    { name: 'list', format: 'list' },
    { name: 'update' },
    { name: 'delete' },
    { name: 'subscribe' }
  ]
};

const apiBuilder = (config: {}) => (dispatch) => {
  const apiConfig = assign(defaultConfig, config);
  const { stream, actionName, actions } = apiConfig;
  let { normalize } = apiConfig;
  const STREAM = actionName ? actionName.toUpperCase() : stream.toUpperCase();

  const callbacks = {};

  // Default normalizers
  if (!normalize) {
    // TODO: stream entity name may be incorrect
    const itemSchema = new schema.Entity(stream, {}, { idAttribute: 'id' });
    const singular = { data: itemSchema };
    const list = { data: new schema.Array(itemSchema) };
    normalize = {
      singular,
      list
    };
  }

  // May do a dynamic message based on the request_id
  // TODO: unregister/watch when needed?
  const unsubscribeMessage = onAction(dispatch)(WEBSOCKET_MESSAGE, (action) => {
    // TODO: if a socket error message, handle == non api message
    const { message } = action.data;
    // wat for now
    if (message.request_id) {
      // handle this better
      const callback = callbacks[message.request_id];
      isFunction(callback) ? callback(message) : console.warn('No callback found for message ', message.request_id);
      delete callbacks[message.request_id];
    }
  });

  const sendAction = (action, options, data) => {
    const requestId = v4();

    // TODO: how to send subscription actions, as they require a slightly
    // different message format.
    return new Promise((resolve, reject) => {
      dispatch({
        type: WEBSOCKET_SEND,
        data: {
          stream,
          payload: {
            action,
            request_id: requestId,
            ...data
          }
        }
      });

      dispatch({
        type: actionTypes[action].REQUEST,
        action,
        requestData: data
      });

      callbacks[requestId] = (message) => {
        // 200, wont work for all requests
        if (message.response_status === 200) {
          const dataSchema = options.format === 'list' ? normalize.list : normalize.singular;
          const messageData = normalizeFunction(message, dataSchema);
          resolve(messageData);
          dispatch({
            type: actionTypes[action].SUCCESS,
            action,
            response: messageData,
            requestData: data
          });
        } else {
          const errors = message;
          reject(errors);
          dispatch({
            type: actionTypes[action].FAILURE,
            action,
            requestData: data,
            errors
          });
        }
      };
    });
  };

  const createActionTypes = (action) => {
    const ACTION = action.toUpperCase();
    const REQUEST = `${ACTION}_${STREAM}_REQUEST`;
    const SUCCESS = `${ACTION}_${STREAM}_SUCCESS`;
    const FAILURE = `${ACTION}_${STREAM}_FAILURE`;
    const types = {
      REQUEST,
      SUCCESS,
      FAILURE
    };
    return types;
  };

  const api = {};
  const actionTypes = {};
  actions.forEach((actionConfig) => {
    // partial doesn't work here for some reason…
    api[actionConfig.name] = partial(sendAction, [actionConfig.name, actionConfig]);
    actionTypes[actionConfig.name] = createActionTypes(actionConfig.name);
  });

  return {
    api,
    actionTypes,
    unsubscribe: unsubscribeMessage
  };
};

export default apiBuilder;
