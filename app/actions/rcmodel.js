import apiBuilder from './api/base';

const rcmodel = apiBuilder({
  stream: 'rcmodel'
});

export default rcmodel;
