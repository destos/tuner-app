// @flow
import React, { Component } from 'react';
import classNames from 'classnames'
// redux
import { connect } from 'react-redux';
// import {} from '../../middleware/serial/actions';

class Status extends Component {
  props: {
    connection: object
    // connected: () => void
  }
  render() {
    let connected = false;
    const { connection } = this.props.connection;

    const statusClass = classNames({
      'connection-indicator': true,
      active: connected
    });
    return (
      <div className={statusClass}>
        <i className="fa fa-plug" aria-hidden="true" />
      </div>
    )
  }
}

function mapStateToProps(state, ownProps) {
  return {
    connection: state.connection
  };
}

const mapPropsToDispatch = (dispatch, ownProps) => {
  return {
    // haltConnections: haltConnections(dispatch),
    // updateConnections: updateConnections(dispatch)
  };
};

export default connect(mapStateToProps, mapPropsToDispatch)(Status);
