// @flow
import React, { Component } from 'react';
import classNames from 'classnames'
// redux
import { connect } from 'react-redux';
import { UncontrolledTooltip } from 'reactstrap/lib/Uncontrolled';


export default class StatusRing extends Component {
  props: {
    activeText: string,
    inactiveText: string,
    active: bool,
    managed: bool,
    // connect: () => void,
    // disconnect: () => void
  }
  render() {
    const {
      active,
      inactiveText,
      activeText,
      disconnect,
      connect
    } = this.props;

    const statusClass = classNames({
      'connection-indicator': true,
      active
    });
    return (
      <div className={statusClass}>
        <i id="StatusRingIcon" className="fa fa-circle-o" aria-hidden="true" />
        <UncontrolledTooltip placement="right" target="StatusRingIcon">
          {active ? activeText : inactiveText}
        </UncontrolledTooltip>
      </div>
    )
  }
}

StatusRing.defaultProps = {
  activeText: 'Connected',
  inactiveText: 'Disconnected',
  active: false,
  managed: false
}
