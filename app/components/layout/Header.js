// @flow
import React, { Component, PropTypes } from 'react';
import type { Children } from 'react';
import Container from 'reactstrap/lib/Container';
import Navbar from 'reactstrap/lib/Navbar';
import NavbarBrand from 'reactstrap/lib/NavbarBrand';
import Collapse from 'reactstrap/lib/Collapse';
// import Nav from 'reactstrap/lib/Nav';
// import NavItem from 'reactstrap/lib/NavItem';
// import NavLink from 'reactstrap/lib/NavLink';

export default class Header extends Component {
  props: {
    children: Children
    // title: PropTypes.string
  };

  render() {
    // We aren't actually toggling this nav…
    return (
      <Navbar className="page-header" toggleable="xs" light color="faded">
        <NavbarBrand>{this.props.title}</NavbarBrand>
        <Collapse isOpen navbar>
          {this.props.children}
        </Collapse>
      </Navbar>
    );
  }
}

// Header.propTypes = {
//   children: PropTypes.element,
//   title: PropTypes.string.isRequired
// }
//
// Header.defaultProps = {
//   children: ''
// }
