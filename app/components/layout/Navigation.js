import React, { Component, PropTypes } from 'react';
import { NavLink } from 'react-router-dom';
import Status from '../connection/Status'
import WebsocketStatus from '../../containers/WebsocketStatus'


class ConnectionIndicator extends Component {
  render() {
    const connectionLink = `/connection/${this.props.rcmodel.id}`
    return (
      <NavLink
        to={connectionLink}
        className="nav-item nav-connection-status"
        activeClassName="active"
      >
        <Status />
      </NavLink>
    )
  }
}
// ConnectionIndicator.propTypes = {
//   rcmodel: PropTypes.object.isRequired
// }

export default class Navigation extends Component {

  getNavElements() {
    const elements = [];

    elements.push(
      <NavLink
        to="/"
        exact
        className="nav-item app-title"
        activeClassName="active"
        key="home"
      >
        <i className="fa fa-home" aria-hidden="true" />
      </NavLink>
    )

    // show all rcmodels
    const rcmodels = this.props.rcmodels || [];
    rcmodels.forEach((rcmodel) => {
      elements.push(<ConnectionIndicator rcmodel={rcmodel} key={rcmodel.id} />)
    });

    elements.push(
      <div
        key="ws-status"
        className="nav-item nav-push-down">
        <WebsocketStatus />
      </div>
    )

    elements.push(
      <NavLink
        to="/settings"
        className="nav-item nav-settings"
        activeClassName="active"
        key="settings"
      >
        <i className="fa fa-cog" aria-hidden="true" />
      </NavLink>
    )
    return elements
  }
  render() {
    // const { activeConnection } = this.state;
    // Get serial list state
    return (
      <nav id="app-nav">
        {this.getNavElements()}
      </nav>
    );
  }
}
