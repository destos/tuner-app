// @flow
import React, { Component } from 'react';
import type { Children } from 'react';
import RCModelNavigation from '../containers/RCModelNavigation'

export default class App extends Component {
  props: {
    children: Children
  };

  render() {
    return (
      <div id="app-container">
        <RCModelNavigation filter="enabled" />
        <div id="page-wrapper">
          {this.props.children}
        </div>
      </div>
    );
  }
}
