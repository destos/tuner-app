// @flow
import React, { Component } from 'react';
import { connect } from 'react-redux';
// import {} from '../middleware/serial/actions';
import Header from '../components/layout/Header';
import { getRcmodel } from '../reducers/rcmodel/byId';


class ConnectionPage extends Component {
  render() {
    const { rcmodel } = this.props;
    return (
      <div>
        <Header title={rcmodel && rcmodel.name || 'none'} />
      </div>
    );
  }
}

// temp
const connectionActions = {};

const mapStateToProps = (state, ownProps) => {
  const id = ownProps.match.params.rcModelId;
  return {
    rcmodel: getRcmodel(state.rcmodel.byId, id),
    connection: null
  }
};

export default connect(mapStateToProps, connectionActions)(ConnectionPage)
