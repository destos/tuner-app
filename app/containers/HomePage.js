// @flow
import React, { Component } from 'react';
// import { Container, Row, Col, Badge, Alert } from 'reactstrap';
import Container from 'reactstrap/lib/Container';
import Row from 'reactstrap/lib/Row';
import Col from 'reactstrap/lib/Col';
// import Badge from 'reactstrap/lib/Badge';
// import Nav from 'reactstrap/lib/Nav';
// import NavItem from 'reactstrap/lib/NavItem';
// import NavLink from 'reactstrap/lib/NavLink';
// alert doesn't seem to be working
// import Alert from 'reactstrap/lib/Alert';
import Header from '../components/layout/Header';

export default class HomePage extends Component {
  render() {

    return (
      <div id="page-container">
        <Header title="Tuner App">
          {/* <Nav navbar>
            <NavItem>
              <Badge color="danger" className="ml-1">Alpha</Badge>
            </NavItem>
            <NavItem>
              <NavLink>Wat</NavLink>
            </NavItem>
          </Nav> */}
        </Header>
        <Container fluid>
          <Row>
            <Col>
              <div className="alert alert-danger">
                This app is currently in alpha status which means a lot of shit will probably be broken.
              </div>
              <h2>Using the app</h2>
              <p>Simply connect your flight controller to your computer and the app should attempt to connect.</p>
              <p>Once it's connected you can edit and sync PIDs and profiles to your flight controller. These profiles are saved to your online profile and in the app. You can make changes to these values while the app is disconnected from your flight controller and they should automatically sync to your flight controller when it is plugged back in.</p>
              <p>These features don't currently exist. Haha.</p>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}
