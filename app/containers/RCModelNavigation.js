import React, { Component } from 'react';
import { connect } from 'react-redux';
import { onAction } from 'redux-action-watch/lib/actionCreators';
import rcmodelActions from '../actions/rcmodel';
import { WEBSOCKET_OPEN } from '../middleware/websocket';
import { getVisibleRcmodels, getErrorMessage, getIsFetching } from '../reducers/rcmodel';

import Navigation from '../components/layout/Navigation'
// TODO: error display component?

class RCModelNavigation extends Component {
  componentDidMount() {
    this.fetchData();
    // watch for websocket_open and call this.fetchData again
    this.unsubscribeOpen = this.props.onAction(WEBSOCKET_OPEN, (action) => {
      this.fetchData();
    });
  }
  componentDidUpdate(prevProps) {
    if (this.props.filter !== prevProps.filter) {
      this.fetchData();
    }
  }
  componentWillUnmount() {
    this.unsubscribeOpen();
    this.props.rcmodel.unsubscribe();
  }
  fetchData() {
    const { filter, rcmodel } = this.props;
    rcmodel.api.list({ filter });
    // .then(() => {
    //   console.log('got it!');
    // })
  }
  render() {
    const { isFetching, errorMessage, rcmodels } = this.props;
    const hasErrors = errorMessage && !rcmodels.length;
    const fetching = isFetching && !rcmodels.length;
    // onRcmodelClick={toggleRcmodel}
    return (
      <Navigation
        rcmodels={rcmodels}
        loading={fetching}
        hasErrors={hasErrors}
      />
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  const filter = ownProps.filter || 'enabled';
  return {
    isFetching: getIsFetching(state, filter),
    errorMessage: getErrorMessage(state, filter),
    rcmodels: getVisibleRcmodels(state, filter),
    filter
  };
};

const mapPropsToDispatch = (dispatch, ownProps) => {
  return {
    onAction: onAction(dispatch),
    rcmodel: rcmodelActions(dispatch)
    // TODO hook up actions as needed
    // rcmodelActions
  };
};

export default connect(mapStateToProps, mapPropsToDispatch)(RCModelNavigation);
