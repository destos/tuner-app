// @flow
import React, { Component } from 'react';
import { Provider } from 'react-redux';
import { ConnectedRouter } from 'react-router-redux';
import Routes from '../routes';
import { setup as setupSerialManager } from '../middleware/serial/actions'
import connectToSocket from '../utils/ws_connect';

// type RootType = {
//   store: {},
//   history: {}
// };

export default class Root extends Component {
  componentWillMount() {
    const { dispatch, getState } = this.props.store;
    const { connection } = getState();
    // possibly turn on serial connection attempts if state allows
    setupSerialManager(dispatch)({
      searching: connection.searching,
    });
    // Connect to websocket
    connectToSocket(dispatch);
  }
  render() {
    const { store, history } = this.props;
    return (
      <Provider store={store}>
        <ConnectedRouter history={history}>
          <Routes />
        </ConnectedRouter>
      </Provider>
    );
  }
}

// Root.propTypes = {
//   store:
// }
