// @flow
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { haltConnections, updateConnections } from '../middleware/serial/actions';

// components
import Container from 'reactstrap/lib/Container';
import Row from 'reactstrap/lib/Row';
import Col from 'reactstrap/lib/Col';
import FormGroup from 'reactstrap/lib/FormGroup';
import Label from 'reactstrap/lib/Label';
import Input from 'reactstrap/lib/Input';
import Header from '../components/layout/Header';


class ConnectionSettings extends Component {
  toggleSearching() {
    const { searching } = this.props.connection;
    const { haltConnections, updateConnections } = this.props;
    if (searching) {
      haltConnections();
    } else {
      updateConnections({ searching: true });
    }
  }
  render() {
    const { connection } = this.props;
    return (
      <FormGroup check>
        <Label check>
          <Input type="checkbox" defaultChecked={connection.searching} onChange={::this.toggleSearching} />{' '}
          Continuous searching
        </Label>
      </FormGroup>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    connection: state.connection
  };
};

const mapPropsToDispatch = (dispatch, ownProps) => {
  return {
    haltConnections: haltConnections(dispatch),
    updateConnections: updateConnections(dispatch)
  };
};

const ConnectedConnectionSettings = connect(
  mapStateToProps, mapPropsToDispatch)(ConnectionSettings);


export default class SettingsPage extends Component {
  render() {
    return (
      <div id="page-container">
        <Header title="Settings"/>
        <Container fluid className="w-100">
          <Row>
            <Col>
              <h3>Connection preferences</h3>
              <ConnectedConnectionSettings />
              <hr/>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}
