import { connect } from 'react-redux';
import StatusRing from '../components/connection/StatusRing';

function mapStateToProps(state, ownProps) {
  return {
    active: state.websocket.connected
  };
}

const actions = {};

export default connect(mapStateToProps, actions)(StatusRing);
