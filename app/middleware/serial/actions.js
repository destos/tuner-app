// @flow
import {
  SETUP_SERIAL_MANAGER,
  UPDATE_CONNECTIONS,
  HALT_UPDATE_CONNECTIONS,
  // TOGGLE_UPDATE_CONNECTIONS,
  NEW_CONNECTION,
  REMOVE_CONNECTION,
  CONNECTION_DISCONNECTED,
  // SELECT_CONNECTION
} from './index';

// handler actions

export const newConnection = ({port, mspPort}) => {
  return {
    type: NEW_CONNECTION,
    data: {
      timestamp: new Date(),
      port
    }
  };
};

export const disconnected = (comName) => {
  return {
    type: CONNECTION_DISCONNECTED,
    data: {
      timestamp: new Date(),
      comName
    }
  };
};

// user actions

export const disconnect = (dispatch) => (comName) => {
  dispatch({
    type: REMOVE_CONNECTION,
    data: {
      timestamp: new Date(),
      comName
    }
  });
};

export const setup = (dispatch) => (config) => {
  dispatch({
    type: SETUP_SERIAL_MANAGER,
    data: {
      timestamp: new Date(),
      config
    }
  });
};

export const updateConnections = (dispatch) => ({ searching }) => {
  dispatch({
    type: UPDATE_CONNECTIONS,
    data: {
      timestamp: new Date(),
      searching
    }
  });
};

export const haltConnections = (dispatch) => () => {
  dispatch({
    type: HALT_UPDATE_CONNECTIONS,
    data: {
      timestamp: new Date()
    }
  });
};

// export function removeConnection =

const actions = {
  newConnection,
  disconnected,
  disconnect,
  setup,
  updateConnections,
  haltConnections
};

export default actions;
