/* @flow */
import { compose } from 'redux';
import defaults from 'lodash/fp/defaults';
import partial from 'lodash/fp/partial';
import { newConnection, disconnected } from './actions';
import ConnectionManager from '../../utils/serial_manager';

// Actions that need performed that modify state
// a new connection appearing
// a connection disappearing
// send/receive communication occuring? probably not

// dispatched by user

// set up the connection manager
export const SETUP_SERIAL_MANAGER = 'SM:SETUP_SERIAL_MANAGER';
// tells the connection manager to start looking for connections
export const UPDATE_CONNECTIONS = 'SM:UPDATE_CONNECTIONS';
// Stops the connection manager from looking for new connections
export const HALT_UPDATE_CONNECTIONS = 'SM:HALT_UPDATE_CONNECTIONS';
// toggler
// export const TOGGLE_UPDATE_CONNECTIONS = 'SM:TOGGLE_UPDATE_CONNECTIONS';
// When we are attempting to remove a connection
export const REMOVE_CONNECTION = 'SM:REMOVE_CONNECTION';

// dispatched by middleware

// A new FC connection was detected by the hander
export const NEW_CONNECTION = 'SM:NEW_CONNECTION';
// Connection lost or successfully removed
export const CONNECTION_DISCONNECTED = 'SM:CONNECTION_DISCONNECTED';
// selects the connection and associated rc_model in the interface?
// export const SELECT_CONNECTION = 'SM:SELECT_CONNECTION';


const createMiddleware = () => {
  let manager = null;

  const initialize = ({ dispatch }, config) => {
    const dispatchAction = partial(compose, [dispatch]);

    const options = {
      onConnect: dispatchAction(newConnection),
      onDisconnect: dispatchAction(disconnected),
    };
    manager = new ConnectionManager(defaults(config, options));
  };

  return (store: Object) => (next: Function) => (action: Action) => {
    switch (action.type) {
      case SETUP_SERIAL_MANAGER:
        const { config } = action.data;
        if (!manager) {
          initialize(store, config);
        }
        next(action);
        break;
      case UPDATE_CONNECTIONS:
        const { searching } = action.data;
        manager.search(searching);
        next(action);
        break;
      case HALT_UPDATE_CONNECTIONS:
        manager.stopSearch();
        next(action);
        break;
      // case NEW_CONNECTION:
      //   debugger;
      //   // manager.connect(action.data.port.comName);
      //   next(action);
      //   break;
      case REMOVE_CONNECTION:
        // TODO: potentially handle this with a promise and dispatch an event when succeeds
        manager.disconnect(action.data.port.comName);
        next(action);
        break;
      default:
        next(action);
    }
  };
};

export default createMiddleware();
