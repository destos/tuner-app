/* @flow */
import { compose } from 'redux';
import partial from 'lodash/fp/partial';
import partialRight from 'lodash/fp/partialRight';
import { connecting, open, closed, message } from './actions';
import { WebSocketBridge } from 'django-channels';

// Action types to be dispatched by the user
export const WEBSOCKET_CONNECT = 'WS:CONNECT';
export const WEBSOCKET_DISCONNECT = 'WS:DISCONNECT';
export const WEBSOCKET_SEND = 'WS:SEND';

// Action types dispatched by the WebSocket implementation
export const WEBSOCKET_CONNECTING = 'WS:CONNECTING';
export const WEBSOCKET_OPEN = 'WS:OPEN';
export const WEBSOCKET_DISCONNECTING = 'WS:DISCONNECTING';
export const WEBSOCKET_CLOSED = 'WS:CLOSED';
export const WEBSOCKET_MESSAGE = 'WS:MESSAGE';


const createMiddleware = () => {
  // Hold a reference to the WebSocket instance in use.
  let bridge = null;

  /**
   * A function to create the WebSocket object and attach the standard callbacks
   */
  const initialize = ({ dispatch }, config: Config) => {
    // Instantiate the websocket.
    bridge = new WebSocketBridge();
    bridge.connect(...config.connectArgs);

    // Function will dispatch actions returned from action creators.
    const dispatchAction = partial(compose, [dispatch]);

    // Dispatch generic action for non-stream messages
    // bridge.listen(dispatchAction(message));
    bridge.socket.onmessage = (event) => {
      const msg = JSON.parse(event.data);
      dispatchAction(message)(msg.payload);
    }

    bridge.socket.onopen = dispatchAction(open);
    bridge.socket.onclose = dispatchAction(closed);

    // An optimistic callback assignment for WebSocket objects that support this
    const onConnecting = dispatchAction(connecting);
    // Add the bridge as the 2nd argument (after the event).
    bridge.socket.onconnecting = partialRight(onConnecting, [bridge]);
  };

  /**
   * Close the WebSocket connection and cleanup
   */
  const close = () => {
    if (bridge) {
      console.warn(`Closing WebSocket connection to ${bridge.socket.url} ...`);
      bridge.socket.close();
      bridge = null;
    }
  };

  /**
   * The primary Redux middleware function.
   * Each of the actions handled are user-dispatched.
   */
  return (store: Object) => (next: Function) => (action: Action) => {
    switch (action.type) {
      // User request to connect
      case WEBSOCKET_CONNECT:
        close();
        initialize(store, action.data);
        next(action);
        break;

      // User request to disconnect
      case WEBSOCKET_DISCONNECT:
        close();
        next(action);
        break;

      // User request to send a message
      case WEBSOCKET_SEND:
        // TODO: may have to do a different connected lookup, bridge will be set even though WS is not available or fully connected
        if (bridge) {
          try {
            bridge.send(action.data);
          } catch (e) {
            console.warn(e);
          }
        } else {
          console.warn('WebSocket is closed, ignoring. Trigger a WEBSOCKET_CONNECT first.');
        }
        next(action);
        break;

      default:
        next(action);
    }
  };
};

export default createMiddleware();
