/* @flow */

declare type Action = { type: string; data: Object };

type Config = {
  url?: string,
  websocket?: WebSocketInterface,
  args?: Array<any>
};

interface WebSocketInterface {
  close(code?: string, reason?: string);
  send(data: string);
}
