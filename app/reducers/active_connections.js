// active connections reducers
// TODO: this isn't done yet
// @flow
import { ADD_PORT, REMOVE_PORT } from '../actions/connection';

// export type activeStateType = {
//   selected: object,
//   connections: array
// };

type actionType = {
  type: string,
  portName: string
};

// export default function connection(state: connectionStateType, action: actionType) {
export default function connection(state = [], action: actionType) {
  switch (action.type) {
    // case UPDATE_CONNECTIONS:
    case ADD_PORT:
      return [
        ...state,
        {
          portName: action.portName,
          active: false
        }
      ]
    case REMOVE_PORT:
      return
    default:
      return state;
  }
}
