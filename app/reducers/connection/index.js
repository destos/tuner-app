// Connection reducers
// @flow
import { combineReducers } from 'redux';
import {
  UPDATE_CONNECTIONS,
  HALT_UPDATE_CONNECTIONS,
  NEW_CONNECTION,
  CONNECTION_DISCONNECTED,
  // CONNECTION_DISCONNECTED,
  // SELECT_CONNECTION
} from '../../middleware/serial';

// export type connectionStateType = {
//   selected: object,
//   connections: array
// };

// type actionType = {
//   type: string
//   connection:
// };

// export default function connection(state: connectionStateType, action: actionType) {
// export default function connection(state = {}, action) {
//   switch (action.type) {
//     // case UPDATE_CONNECTIONS:
//     case SELECT_CONNECTION:
//       return {
//         ...state,
//         connection: action.connection
//       }
//     default:
//       return state;
//   }
// }

export const activeConnections = (state = [], action) => {
  switch (action.type) {
    case NEW_CONNECTION:
      return [...state, action.data.port.comName];
    case CONNECTION_DISCONNECTED:
      return state.filter((id) => id !== action.data.comName);
    default:
      return state;
  }
};

export const searching = (state = true, action) => {
  // searching is on by default
  switch (action.type) {
    // Set state to whether we are doing a continuous search or not.
    case UPDATE_CONNECTIONS:
      return action.data.searching;
      break;
    case HALT_UPDATE_CONNECTIONS:
      return false;
      break;
    default:
      return state;
      break;
  }
};

const connection = combineReducers({
  active: activeConnections,
  searching
});

export default connection;
