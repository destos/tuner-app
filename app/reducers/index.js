// @flow
import { combineReducers } from 'redux';
import { routerReducer as router } from 'react-router-redux';
import actionWatchReducer from 'redux-action-watch/lib/reducer';
import connection from './connection';
import websocket from './websocket';
import rcmodel from './rcmodel';

const rootReducer = combineReducers({
  connection,
  websocket,
  rcmodel,
  router,
  'watcher': actionWatchReducer
});

export default rootReducer;
