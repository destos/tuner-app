const byId = (state = {}, action) => {
  if (action.response) {
    return {
      ...state,
      ...action.response.entities.rcmodel
    };
  }
  return state;
};

export default byId;

export const getRcmodel = (state, id) => state[id];
