import { combineReducers } from 'redux';

const createList = (filter) => {
  // const handleToggle = (state, action) => {
  //   const { result: toggleId, entities } = action.response;
  //   const { completed } = entities.todos[toggleId];
  //   const shouldRemove = (
  //     (completed && filter === 'active') ||
  //     (!completed && filter === 'completed')
  //   );
  //
  //   return shouldRemove ?
  //     state.filter(id => id !== toggleId) :
  //     state;
  // };

  const ids = (state = [], action) => {
    switch (action.type) {
      case 'LIST_RCMODEL_SUCCESS':
        return filter === action.requestData.filter ?
          action.response.result.data :
          state;
      case 'ADD_RCMODEL_SUCCESS':
        return filter !== 'completed' ?
          [...state, action.response.result.data] :
          state;
      // case 'TOGGLE_RCMODEL_SUCCESS':
      //   return handleToggle(state, action);
      default:
        return state;
    }
  };

  const isFetching = (state = false, action) => {
    if (action.filter !== filter) {
      return state;
    }
    switch (action.type) {
      case 'LIST_RCMODEL_REQUEST':
        return true;
      case 'LIST_RCMODEL_SUCCESS':
      case 'LIST_RCMODEL_FAILURE':
        return false;
      default:
        return state;
    }
  };

  const errorMessage = (state = null, action) => {
    if (filter !== action.filter) {
      return state;
    }
    switch (action.type) {
      case 'LIST_RCMODEL_FAILURE':
        return action.message;
      case 'LIST_RCMODEL_REQUEST':
      case 'LIST_RCMODEL_SUCCESS':
        return null;
      default:
        return state;
    }
  };

  return combineReducers({
    ids,
    isFetching,
    errorMessage
  });
};

export default createList;

export const getIds = (state) => state.ids;
export const getIsFetching = (state) => state.isFetching;
export const getErrorMessage = (state) => state.errorMessage;
