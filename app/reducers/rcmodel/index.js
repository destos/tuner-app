import { combineReducers } from 'redux';
import byId, * as fromById  from './byId';
import createList, * as fromList from './createList';

const listByFilter = combineReducers({
  all: createList('all'),
  enabled: createList('enabled'),
  archived: createList('archived')
});

const rcmodel = combineReducers({
  byId,
  listByFilter
});

export default rcmodel;

export const getVisibleRcmodels = (state, filter) => {
  const ids = fromList.getIds(state.rcmodel.listByFilter[filter]);
  return ids.map(id => fromById.getRcmodel(state.rcmodel.byId, id));
};

export const getIsFetching = (state, filter) =>
  fromList.getIsFetching(state.rcmodel.listByFilter[filter]);

export const getErrorMessage = (state, filter) =>
  fromList.getErrorMessage(state.rcmodel.listByFilter[filter]);
