// Connection status reducers for the websocket connection
import { combineReducers } from 'redux';
import { WEBSOCKET_OPEN, WEBSOCKET_CLOSED } from '../middleware/websocket';

export const connected = (state = false, action) => {
  // searching is on by default
  switch (action.type) {
    // Set state to whether we are doing a continuous search or not.
    case WEBSOCKET_OPEN:
      return true;
    case WEBSOCKET_CLOSED:
      return false;
    default:
      return state;
  }
};

const websocket = combineReducers({
  connected
});

export default websocket;
