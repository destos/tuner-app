/* eslint flowtype-errors/show-errors: 0 */
import React from 'react';
import { Switch, Route } from 'react-router';

import App from './containers/App';

import HomePage from './containers/HomePage';
import ConnectionPage from './containers/ConnectionPage';
import SettingsPage from './containers/SettingsPage';

export default () => (
  <App>
    <Switch>
      <Route exact path="/" component={HomePage} />
      <Route path="/settings" component={SettingsPage} />
      <Route path="/connection/:rcModelId" component={ConnectionPage} />
    </Switch>
  </App>
);
