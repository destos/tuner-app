import { createStore, applyMiddleware, compose } from 'redux';
// middleware
import thunk from 'redux-thunk';
import { createHashHistory } from 'history';
import { routerMiddleware } from 'react-router-redux';
import promiseMiddleware from 'redux-promise';
import { createLogger } from 'redux-logger';
import actionWatchMiddlewaregenerator from 'redux-action-watch/lib/middleware';
import websocket from '../middleware/websocket';
import serial from '../middleware/serial';

// reducers/actions
import rootReducer from '../reducers';
import { routerActions } from 'react-router-redux';
import rcmodelActions from '../actions/rcmodel';
// middleware reducers/actions
import connectionActions from '../middleware/serial/actions';

const history = createHashHistory();

const configureStore = (initialState) => {
  // Redux Configuration
  const middleware = [];
  const enhancers = [];

  // promise support middleware
  // Is this the same as the thunk middlware?
  middleware.push(promiseMiddleware);

  // Thunk Middleware
  middleware.push(thunk);

  // Logging Middleware
  const logger = createLogger({
    level: 'info',
    collapsed: true
  });
  middleware.push(logger);

  // Router Middleware
  const router = routerMiddleware(history);
  middleware.push(router);

  // WS middleware
  middleware.push(websocket);

  // Serial connection middleware
  middleware.push(serial);

  // Action wather middleware
  const actionWatchMiddleware = actionWatchMiddlewaregenerator('watcher');
  middleware.push(actionWatchMiddleware);

  // Redux DevTools Configuration
  const actionCreators = {
    ...connectionActions,
    ...rcmodelActions,
    ...routerActions,
  };
  // If Redux DevTools Extension is installed use it, otherwise use Redux compose
  /* eslint-disable no-underscore-dangle */
  const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
    ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({
      // Options: http://zalmoxisus.github.io/redux-devtools-extension/API/Arguments.html
      actionCreators,
    })
    : compose;
  /* eslint-enable no-underscore-dangle */

  // Apply Middleware & Compose Enhancers
  enhancers.push(applyMiddleware(...middleware));
  const enhancer = composeEnhancers(...enhancers);

  // Create Store
  const store = createStore(rootReducer, initialState, enhancer);

  if (module.hot) {
    module.hot.accept('../reducers', () =>
      store.replaceReducer(require('../reducers')) // eslint-disable-line global-require
    );
  }

  return store;
};

export default { configureStore, history };
