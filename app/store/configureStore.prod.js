// @flow
import { createStore, applyMiddleware } from 'redux';
// middleware
import thunk from 'redux-thunk';
import promiseMiddleware from 'redux-promise';
import { createBrowserHistory } from 'history';
import { routerMiddleware } from 'react-router-redux';
import actionWatchMiddlewaregenerator from 'redux-action-watch/lib/middleware';
import websocket from '../middleware/websocket';
// reducers
import rootReducer from '../reducers';

const history = createBrowserHistory();
const router = routerMiddleware(history);
const actionWatchMiddleware = actionWatchMiddlewaregenerator('watcher');
const enhancer = applyMiddleware(promiseMiddleware, thunk, router, websocket, actionWatchMiddleware);

function configureStore(initialState) {
  return createStore(rootReducer, initialState, enhancer);
}

export default { configureStore, history };
