import Message from '../message'

export default class BaseIn extends Message{
  messageId: null
  inputData: null
  constructor(...args) {
    super();
    this.inputData = args;
  }
  getData() {
    return {};
  }
  getMessage() {
    return {
      id: this.messageId,
      data: this.getData(...this.inputData)
    };
  }
}
