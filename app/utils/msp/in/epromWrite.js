import BaseIn from './base'

export default class EpromWrite extends BaseIn {
  messageId: 251
  getData() {
    return new Buffer(1);
  }
}
