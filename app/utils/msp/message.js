import _ from 'lodash';

export default class Message {
  static header = [0x24, 0x4D, 0x3C]
  static headerPosition = 0
  static lengthPosition = 3
  static messageIdPosition = 4
  static dataPosition = 5

  // hold onto the message in the instance
  messageBuffer: null
  failed: false
  promise: null

  constructor() {
    this.promise = new Promise((resolve, reject) => {
      // Why does native not attach these to
      this.resolve = resolve;
      this.reject = reject;
    });
    this.promise.catch(this.failed);
  }

  buildMessage() {
    const message = this.getMessage();
    let messageId;
    let length;
    let size;
    let data;

    // just sending a command number, auto-create message
    if (_.isNumber(message)) {
      messageId = message;
      length = 0;
      size = 7;
      // data = null;
      data = 0;
    // Sending a message with data
    } else if (_.isObject(message)) {
      messageId = message.id;
      data = message.data;
      length = data.length;
      // header + length + messageId offset is 7
      size = 7 + length;
    }

    const msgBuffer = new Buffer(size);
    // put header message at the front
    new Buffer(Message.header).copy(msgBuffer, Message.headerPosition);
    msgBuffer.writeUInt8(length, Message.lengthPosition);
    msgBuffer.writeUInt8(messageId, Message.messageIdPosition);

    // TODO: use flow to assert data is a buffer?
    if (data) {
      data.copy(msgBuffer, Message.dataPosition);
    }

    // write out checksum
    const checksumPosition = Message.dataPosition + length;
    msgBuffer.writeUInt8(Message.checksum(msgBuffer), checksumPosition);
    // ending bit? apparently MSP needs it to end the message. TODO: test
    msgBuffer.writeUInt8(0, 6 + length);

    // save the message
    this.messageBuffer = msgBuffer;
    return msgBuffer;
  }

  parseData(data) {
    this.rawData = data;
    this.parsedData = this.parse(data);
    return this.parsedData;
  }

  parse(data) {
    console.info('should add a parse method');
    console.log(data);
  }

  failed(reason) {
    // mark the message as failed, maybe reject a promise too.
    this.failed = true;
  }

  static checksum(buffer) {
    const data = Array.prototype.slice.call(buffer, 3);
    // If no data is passed for our message
    if (data[0] === 0) {
      // out message
      return data[1];
    }
    // in message
    // grab only message and
    return data
      .slice(0, data[0])
      .reduce((tot, cur) => {
        return tot ^ cur;
      }, 0);
  }
}
