import SerialPort from 'serialport';
import assign from 'lodash/fp/assign';
import head from 'lodash/fp/head';
import drop from 'lodash/fp/drop';
import pull from 'lodash/fp/pull';
import forEach from 'lodash/fp/forEach';
import isFunction from 'lodash/fp/isFunction';
import { DefaultDict } from 'pycollections';

const OutTypes = require('./out');
const InTypes = require('./in');

class MSP {
  // TODO: do we have checks to see what version of the protocol is supported and handle
  // access/warnings to those types of messages?

  static defaults = {
    baudrate: 115200,
    databits: 8,
    stopbits: 1,
    parity: 'none',
    parser: SerialPort.parsers.raw
  }

  constructor(portMeta) {
    this.portMeta = portMeta;
    // bind methods
    this.onOpen = ::this.onOpen;
    this.onData = ::this.onData;
    this.onError = ::this.onError;
    this.onClose = ::this.onClose;

    this.messages = {};
    this.readMessages = {};

    this.messageQueue = new DefaultDict([].constructor);

    // Out is data from the FC
    // In is data to the FC
    forEach(InTypes, (Type, key) => {
      this.addInMessage(key, Type);
    });

    this.promise = new Promise((resolve, reject) => {
      // Why does native not attach these to
      this.resolve = resolve;
      this.reject = reject;
    });
  }

  setCloseCallback(callback) {
    if (isFunction(callback)) {
      this.closeCallback = callback;
    }
  }

  addInMessage(name, Type) {
    this[name] = (...args) => {
      // newArguments = Array.prototype.slice.call(args);
      const newMessage = new Type(...args);
      return this.send(newMessage);
    }
  }

  connect(options: {} = {}) {
    // let options = {};
    const newOptions = assign(MSP.defaults, options);
    this.port = new SerialPort(this.portMeta.comName, newOptions);
    this.port.on('open', this.onOpen);
    this.port.on('error', this.onError);
    return this.promise;
  }

  onOpen() {
    // setup callbacks for connection data and errors
    this.port.on('close', this.onClose);
    this.port.on('data', this.onData);
    // resolve the connection promise
    this.resolve(this);
  }

  onData(buffer) {
    const len = buffer[3];
    const type = buffer[4];
    const data = buffer.slice(5, 5 + len);
    // Are there any waiting messages that need to parse the returned data?
    // const Message = this.messages[type];
    const queue = this.messageQueue.get(type, null);
    if (queue && queue.length > 0) {
      const message = head(queue);
      const parsedData = message.parseData(data);
      message.resolve(parsedData);
      // drop the message from the queue, first item is removed
      this.messageQueue.set(type, drop(queue));
    } else {
      console.log('got some unasked for data', buffer);
    }
      // console.log(type, data, message);
  }

  onError(err) {
    if (err.errno === -1 && err.code === 'UNKNOWN') {
      debugger;
      this.disconnect();
    }
    this.reject(err);
  }

  onClose(err) {
    if (err && err.disconnected) {
      debugger;
      // IS this still a case?
      // closed accidentally ( via a unplug .etc )
      // TODO Emit something?
    }
    if (this.closeCallback) {
      this.closeCallback(err);
    }
  }

  // Send a read request message
  read(messageName) {
    // instantiate new message
    const newMessage = new OutTypes[messageName]();
    return this.send(newMessage);
  }

  // TODO: assert message type is correct
  send(message) {
    let msgBuffer;
    if (!message.messageBuffer) {
      msgBuffer = message.buildMessage();
    } else {
      msgBuffer = message.messageBuffer;
    }
    // Put message onto queue for type so we can fulfill them in order
    this.messageQueue.get(message.messageId).push(message);
    this.port.write(msgBuffer, (err) => {
      if (err) {
        message.promise.reject(err);
        this.removeMessage(message);
      }
    });
    return message;
  }

  removeMessage(message) {
    const queue = this.messageQueue.get(message.messageId, null);
    // remove message from the queue whereever it is
    if (queue && queue.length) {
      this.messageQueue.set(message.messageId, pull(queue, message));
    }
  }

  disconnect() {
    if (this.port.isOpen()) {
      this.port.close();
    }
  }
}

export default MSP
