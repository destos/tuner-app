import BaseOut from './base'

export default class Altitude extends BaseOut {
  messageId = 109;

  parse(data) {
    return {
      estimatedAltitude: data.readUInt32LE(0)
    }
  }
}
