import BaseOut from './base'

export default class Atitude extends BaseOut {
  messageId = 108;

  parse(data) {
    return {
      angles: [
        data.readUInt16LE(0),
        data.readUInt16LE(2)
      ],
      heading: data.readUInt16LE(4),
      headFreeModeHold: data.readUInt16LE(6)
    }
  }
};
