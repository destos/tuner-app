import Message from '../message'

export default class BaseOut extends Message {
  messageId = null

  getMessage() {
    return this.messageId;
  }
}
