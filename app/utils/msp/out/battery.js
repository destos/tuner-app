import BaseOut from './base'

export default class Battery extends BaseOut {
  messageId = 110;

  parse(data) {
    return {
      vbat: data.readUInt8(0),
      intPowerMeterSum: data.readUInt16LE(1)
    }
  }
};
