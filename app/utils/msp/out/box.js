import BaseOut from './base'

export default class Box extends BaseOut {
  messageId = 113;

  static names = [
    'ACC',
    'BARO',
    'MAG',
    'CAMSTAB',
    'CAMTRIG',
    'ARM',
    'GPS HOME',
    'GPS HOLD',
    'PASSTHRU',
    'HEADFREE',
    'BEEPER',
    'LEDMAX',
    'LLIGHTS',
    'HEADADJ'
  ];

  parse(data) {
    return {
      checkboxItems: Box.names.reduce(function (obj, name, ix) {
        obj[name] = data.readInt16LE(ix*2);
        return obj;
      }, {})
    };
  }
}
