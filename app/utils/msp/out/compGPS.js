import BaseOut from './base'

export default class CompGPS extends BaseOut {
  messageId = 107

  parse(data) {
    return {
      distanceToHome: data.readUInt16LE(0),
      directionToHome: data.readUInt16LE(2),
      update: data.readUInt8(4)
    }
  }
};
