import BaseOut from './base'

export default class Ident extends BaseOut {
  messageId = 100

  static types = [
    'UNKNOWN',
    'TRI',
    'QUADP',
    'QUADX',
    'BI',
    'GIMBAL',
    'Y6',
    'HEX6',
    'FLYING_WING',
    'Y4',
    'HEX7X',
    'OCTOX8',
    'OCTOFLATP',
    'OCTOFLATX'
  ];

  // TODO: store to message instance
  parse(data) {
    return {
      version: data.readUInt8(0),
      multitype: Ident.types[data.readUInt8(1)],
      mspVersion: data.readUInt8(2),
      capability: data.readUInt32LE(3)
    }
  }
}
