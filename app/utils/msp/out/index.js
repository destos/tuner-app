import Altiude from './altitude';
import Attitude from './attitude';
import Battery from './battery';
import Box from './box';
import CompGPS from './compGPS';
import Ident from './ident';
import Misc from './misc';
import Motor from './motor';
import MotorPins from './motorPins';
import Pid from './pid';
import RawGPS from './rawGPS';
import RawIMU from './rawImu';
import Rc from './rc';
import RcTuning from './rcTuning';
import Servo from './servo';
import Status from './status';

export default {
  altitude: Altiude,
  attitude: Attitude,
  battery: Battery,
  box: Box,
  compGPS: CompGPS,
  ident: Ident,
  misc: Misc,
  motor: Motor,
  motorPins: MotorPins,
  pid: Pid,
  rawGPS: RawGPS,
  rawIMU: RawIMU,
  rc: Rc,
  rcTuning: RcTuning,
  servo: Servo,
  status: Status
};
