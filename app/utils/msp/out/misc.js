import BaseOut from './base'

export default class Misc extends BaseOut {
  messageId = 114

  parse(data) {
    return {
      powerTrigger: data.readUInt16LE(0)
    }
  }
}
