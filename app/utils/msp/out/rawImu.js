import BaseOut from './base'

export default class RawIMU extends BaseOut {
  messageId = 102

  parse(data) {
    return {
      accSmooth: [
        data.readUInt16LE(0),
        data.readUInt16LE(2),
        data.readUInt16LE(4)
      ],
      gyroData: [
        data.readUInt16LE(6),
        data.readUInt16LE(8),
        data.readUInt16LE(10)
      ],
      magADC: [
        data.readUInt16LE(12),
        data.readUInt16LE(14),
        data.readUInt16LE(16)
      ]
    }
  }
}
