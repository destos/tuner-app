import BaseOut from './base'

export default class Status extends BaseOut {
  messageId = 101

  parse(data) {
    return {
      cycleTime: data.readUInt16LE(0),
      i2cErrorCount: data.readUInt16LE(2),
      sensor: data.readUInt16LE(4),
      flags: data.readUInt32LE(6)
    }
  }
}
