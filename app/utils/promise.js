export const firstCompleted = (...promises) => {
  // Promise.all(promises).catch((err) => {
  //   // if any of them get rejected
  // });
  return new Promise((resolve, reject) => {
    const resolvedFirst = (data) => {
      // This is failing
      resolve(data);
      // unset resolve, no longer can be used in this scope
      resolve = () => console.log(promise, 'resolved, but it was too late!');
    }
    promises.forEach((promise) => {
      // every promise gets a chance to be first…
      promise
        .then(resolvedFirst)
        .catch((err) => {
          reject(err)
        });
    });
  });
}
