import SerialPort from 'serialport';
import defaults from 'lodash/fp/defaults';
import partial from 'lodash/fp/partial';
import _ from 'lodash';
import promiseAny from 'p-any';
import MSP from './msp';
import { firstCompleted } from './promise';

// What it needs to do:
// turn on and off active searching.
// hold list of previously tried serial ports and ignore those?
// emits or calls a callback when a new MSP serial connection is established with connection
// handler and details
const baudRates = [
  // 110,
  // 300,
  // 1200,
  // 2400,
  // 4800,
  9600,
  14400,
  19200,
  38400,
  57600,
  115200
];

class ConnectionManager {

  // These may not be the best things to filter by. May have to forgoethe sig test.
  static supportedManufacturers = [
    'BetaFlight',
    'CleanFlight',
    'RaceFlight',
    'STMicroelectronics'
  ]

  static defaultOptions = {
    searching: true,
    // onConnect: (data) => undefined,
    // onDisconnect: (comName) => undefined,
    signatureTest: (port) => {
      return new Promise((resolve, reject) => {
        // Is this in the banned list of connections?
        // const isBlacklisted = indexOf(blacklist, port.comName) >= 0;
        // Does it look like a valid connection?
        const isUnsupported = ConnectionManager.supportedManufacturers.indexOf(port.manufacturer) === -1;
        if (isUnsupported) {
          reject(`port "${port.comName}" manufacturer not supported`);
          return;
        }
        resolve(true);
      });
    },
    connectionTest: (mspPort) => {
      return new Promise((resolve, reject) => {
        // If we get a successful ident response pass
        // Await syntax didn't work :/
        // try {
        //   const message = await mspPort.read('ident');
        //   // TODO: DO more verification
        // } catch (e) {
        //   mspPort.disconnect();
        // }
        const identMessage = mspPort.read('ident');
        identMessage.promise.then((message) => {
          // success ident test
          resolve(message);
        }).catch((e) => {
          reject(e);
          mspPort.disconnect();
        });
      });
    },
    // startSearch: false,
    betweenSearches: 5000,
    blackListAttemptLimit: 3
  }
  // Try every five seconds to connect to new serial ports.
  searching = false
  searchTimeout = 0
  // TODO: also handle connections that have been attempted a lot and blacklist them.
  connections = {}
  blackListCount = {}

  constructor(options) {
    this.options = defaults(options, ConnectionManager.defaultOptions);

    // bindings to this
    this.connect = ::this.connect;
    this.search = ::this.search;
    this.onDisconnect = ::this.onDisconnect;
    if (this.options.searching) {
      this.search(true);
    }
  }
  incrementBlackList(port) {
    console.info(`incrementing blacklist count for ${port.comName}`);
    this.blackListCount[port.comName] += 1;
  }
  isBlacklisted(port) {
    return this.blackListCount[port.comName] > this.options.blackListAttemptLimit;
  }
  attemptConnection() {
    console.log('getting new list of ports');
    const connectionFilter = (port) => {
      // search for portname already in connection pool;
      if (this.isBlacklisted(port.comName)) {
        return false;
      }
      if (port.comName in this.connections) {
        return false;
      }
      if (!port.manufacturer) {
        return false;
      }
      return true;
    };

    SerialPort.list((err, ports) => {
      // TODO: handle serial list errors

      // Get list of all valid connections ( non-blacklisted, etc. )
      const connectionAttempts = _.chain(ports).filter(connectionFilter).map(this.connect).value();

      if (connectionAttempts.length === 0) {
        console.log('no valid ports found.');
        return;
      }

      // Connect to first successful.
      promiseAny(connectionAttempts).then(({port, mspPort}) => {
      // firstCompleted(...foundConnections).then((data) => {
        this.options.onConnect({port, mspPort});
        const name = port.comName;
        this.connections[name] = mspPort;
        return port;
      }).catch((err) => {
        // Trigger something else
        console.error(err);
      });
    });
  }
  search(continuous = true) {
    clearTimeout(self.searchTimeout);
    if (continuous) {
      this.searching = true;
    }
    this.attemptConnection();
    if (!this.searching) {
      return;
    }
    if (this.searching) {
      this.searchTimeout = setTimeout(() => this.search(false), this.options.betweenSearches);
    }
  }
  stopSearch() {
    this.searching = false;
  }
  connect(port) {
    console.info(`attempting connection to "${port.comName}"`);
    let mspPort = null;
    return new Promise((resolve, reject) => {
      // if connection name is in connections pool reject right away
      // TODO: disconnect on failure to connect, register attempt and block attempts after
      // a certain amount of failures.
      // Dispatch actions for those failures so it can be showed in the interface.

      this.options.signatureTest(port).then((testResult) => {
        if (testResult) {
          mspPort = new MSP(port);
          // TODO: close the connection on failure
          // may have to wrap reject to return an object with the mspport so it can be disconnected?
          // Ok try this, we attempt to connect consecuitively ay different baud rates
          // By chaining promise connects with different baud rate configs.
          // We then increment and blacklist connection names that fail more than
          // a certain amount.
          mspPort.setCloseCallback(partial(this.onDisconnect, [port]));
          // const connectOptions = {
          //   // try different connection rates and pass it to a first resolve
          // };
          mspPort.connect().then(() => {
            this.options.connectionTest(mspPort).then(() => {
              resolve({ port, mspPort });
            }).catch(reject);
          }).catch(reject);
        };
      }).catch(reject);

    // Disconnect from port if we run into an error and increment the blacklist
    // TODO: This could probably be done better.
    }).catch((err) => {
      this.incrementBlackList(port);
      if (mspPort) {
        mspPort.disconnect();
      }
    });
  }
  // My beautiful prince, try to get async/await working again
  // async connect(port) {
  //   return new Promise((resolve, reject) => {
  //     // if connection name is in connections pool reject right away
  //     try {
  //       await this.options.signatureTest(port);
  //       const mspPort = new MSP(port);
  //       await mspPort.connect();
  //       await this.options.connectionTest(mspPort);
  //       resolve({mspPort, data: testResult});
  //       return mspPort;
  //     } catch (e) {
  //       // this.blacklist.push(port.comName);
  //       reject(e);
  //     }
  //   });
  // }
  onDisconnect(port, err) {
    console.log(`disconnecting from ${port.comName}`);
    this.options.onDisconnect(port.comName);
    delete this.connections[port.comName];
  }
  disconnect(comName) {
    return new Promise((resolve, reject) => {
      // disconnect fires our local onDisconnect callback on success.
      // TODO: get notified from disconnect call on msp port if success/fail. resolve accordingly
      this.connections[comName].disconnect();
      resolve();
    });
  }
}

export default ConnectionManager
