import { WEBSOCKET_CONNECT } from '../middleware/websocket';

const server = '127.0.0.1:8000';
const protocol = 'ws';
const options = {
  // maxRetries: 5,
  connectionTimeout: 1000 * 60 * 10, // 10 minute timeout for dev
  debug: true,
};

const connectToSocket = (dispatch) => {
  const args = [`${protocol}://${server}/stream/`, [], options];
  dispatch({
    type: WEBSOCKET_CONNECT,
    data: {
      connectArgs: args
    }
  });
}

export default connectToSocket;
